/**
 * BottleDispenser.java
 * 13.9.2017
 */
package vt3;

import java.text.DecimalFormat;
/**
 * @author Harri
 *
 */
import java.util.ArrayList;

public class BottleDispenser {
    
    private int bottles;
    private ArrayList<Bottle> bottle_array;
    private double money;
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        bottle_array = new ArrayList<Bottle>();
        // Add Bottle-objects to the array
        //for(int i = 0;i<bottles;i++) {
            // Use the default constructor to create new Bottles
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        //}
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lis�� rahaa laitteeseen!");
    }
    
    public void buyBottle(int index) {
    	if (money < bottle_array.get(index -1).getPrice()) {
    		System.out.println("Sy�t� rahaa ensin!");
    	}else if (bottles <= 0) {
    		System.out.println("Pullot meni negatiiviseksi, t�yt�!");
        	bottles = 0;
    	}else {
    		money -= bottle_array.get(index - 1).getPrice();
    		System.out.println("KACHUNK! " + bottle_array.get(index - 1).getName() + " tipahti masiinasta!");
    		bottles -= 1;
    		deleteBottle(index);
    	}    

    }
    
    public void returnMoney() {
    	DecimalFormat df = new DecimalFormat("#0.00");
    	// Type cast -shit to avoid Viope errors
    	//Double dmoney = new Double(money);
    	//dmoney = (double)Math.round(dmoney * 100) / 100; 
    	String moneyString = "";
    	moneyString = df.format(money);
        System.out.println("Klink klink. Sinne meniv�t rahat! Rahaa tuli ulos " + moneyString.replace(".", ",")+ "�");
        money = 0;
    }
    
    public void listBottles() {
    	for(int i = 0; i < bottle_array.size(); i++) {
    		System.out.println((i + 1) + ". Nimi: " + bottle_array.get(i).getName());
    		System.out.println("\tKoko: " + bottle_array.get(i).getEnergy() + "\tHinta: " + bottle_array.get(i).getPrice());
    	}
    }
    
    private void deleteBottle(int index) {
        bottle_array.remove(index - 1);
    }

}

