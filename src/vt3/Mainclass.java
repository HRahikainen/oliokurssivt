/**
 * Mainclass.java
 * 13.9.2017
 */
package vt3;

import java.util.Scanner;

/**
 * @author Harri
 *
 */
public class Mainclass {

	public static void main(String[] args) {
		
		BottleDispenser pullomasiina = new BottleDispenser();
		Scanner scan = new Scanner(System.in);
		String choice = "";
		
		// Menu to run the dispenser operations
		do {
			System.out.println("\n*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lis�� rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			
			choice = scan.nextLine();
			
			if (choice.equals("0")) {
				break;
			}else if (choice.equals("1")) {
				pullomasiina.addMoney();
			}else if (choice.equals("2")) {
				pullomasiina.listBottles();
				System.out.print("Valintasi: ");
				String pullo = scan.nextLine();
				pullomasiina.buyBottle(Integer.parseInt(pullo));
			}else if (choice.equals("3")) {
				pullomasiina.returnMoney();
			}else if (choice.equals("4")) {
				pullomasiina.listBottles();
			}else {
				System.out.println("V��r� valinta!");
			}
		}while (!choice.equals("0"));
		
		scan.close();
				
	}

}
