/**
 * Dog.java
 * 8.9.2017
 */
  
package vt2;

import java.util.Scanner;

/**
 * @author Harri
 *
 */
public class Dog {
	
	private String name;
	private String says;
	
	public Dog(String dogName) {
		says = "Much wow!";
		
		// All spaces becomes empty with trim(), which catches invalid inputs
		if (dogName.trim().isEmpty()) {
			name = "Doge";
		}else {
			name = dogName;
		}
		
		System.out.println("Hei, nimeni on " + name + "!");
	}
	
	public void speak(String saying) {
		if (saying.trim().isEmpty()) {
			Scanner sc = new Scanner(System.in);
			
			 do {
				System.out.println(name + ": " + says);
				System.out.print("Mit� koira sanoo: ");
				saying = sc.nextLine();
			}while (saying.trim().isEmpty());
			sc.close();
		}
		
		says = saying;
		System.out.println(name + ": " + says);
		
		Scanner boolIntIdScan = new Scanner(says);
		
		while (boolIntIdScan.hasNext()) {
			if (boolIntIdScan.hasNextBoolean()) {
				System.out.println("Such boolean: " + boolIntIdScan.nextBoolean());
			}else if (boolIntIdScan.hasNextInt()) {
				System.out.println("Such integer: " + boolIntIdScan.nextInt());
			}else {
				System.out.println(boolIntIdScan.next());
			}
		}
		boolIntIdScan.close();
	}
}
