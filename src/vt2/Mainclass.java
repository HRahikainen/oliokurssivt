/*
 * Mainclass.java
 * 8.9.2017
*/

package vt2;

import java.util.Scanner;

/**
 * @author Harri
 *
 */
public class Mainclass {

	public static void main(String[] args) {
		
		String name;
		String saying;
		Scanner scan = new Scanner(System.in);
		//Scanner scan = new Scanner(System.in).useDelimiter("\n");
		
		System.out.print("Anna koiralle nimi: ");
		name = scan.nextLine();
		
		Dog doggo = new Dog(name);
		
		System.out.print("Mit� koira sanoo: ");
		saying = scan.nextLine();
		
		doggo.speak(saying);

		/*
	 	 * Dog rekku = new Dog("Rekku");
		 * Dog musti = new Dog("Musti");
		 * rekku.speak("Hau!");
		 * musti.speak("Vuh!");
		 */
		scan.close();
	}

}
