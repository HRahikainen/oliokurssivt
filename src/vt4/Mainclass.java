/**
 * Mainclass.java
 * 21.9.2017
 */
package vt4;

/**
 * @author Harri
 *
 */
public class Mainclass {

	public static void main(String[] args) {

		/*
		 * String inputFile = "./src/vt4/input.txt";
		String outputFile = "./src/vt4/output.txt";
		
		ReadAndWriteIO io = new ReadAndWriteIO();
		//String resultString = io.readFile(inputFile);
		io.readAndWrite(inputFile, outputFile);

		// System.out.println( System.getProperty( "user.dir" ) );
		// Good ol' dirty solution, also just print() to remove the trailing newline
		//System.out.println(resultString.trim());
		 * */
		String zipName = "./zipinput.zip";
		ReadAndWriteIO io = new ReadAndWriteIO();
		String result = io.readTxtFromZip(zipName);
		System.out.println(result.trim());
	}

}
