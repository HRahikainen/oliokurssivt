/**
 * ReadAndWriteIO.java
 * 21.9.2017
 */
package vt4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author Harri
 *
 */
public class ReadAndWriteIO {
	
	public ReadAndWriteIO() {
		
	}
	
	public String readFile(String filepath) {
		String output = "";
		
		try {
			BufferedReader in; 
			String inputLine;
			
			in = new BufferedReader(new FileReader(filepath));
			while((inputLine = in.readLine()) != null) {
    			output += inputLine + "\n";
			}
			in.close();
			
		} catch (IOException ioe) {
			System.out.println("File not found!");
		}
		return output;
		
	}
	
	public void readAndWrite(String inFile, String outFile) {
		
		try {
				BufferedReader in; 
    			BufferedWriter out;
    
    			in = new BufferedReader(new FileReader(inFile));
    			out = new BufferedWriter(new FileWriter(outFile));
    			String inputLine;
    
    			while((inputLine = in.readLine()) != null) {
    				if ((inputLine.trim().length() < 30) && (inputLine.trim().length() > 0) && (inputLine.contains("v"))) {
    					out.write(inputLine);
            			out.newLine();
    				}
    			}
    			in.close();
    			out.close();
		} catch (IOException ex) {
    			System.out.println("File not found!");
		}
	}
	
	public String readTxtFromZip(String archiveName) {
		// Read a single .txt file from specified zip archive and return contents
		String output = "";
		String inputLine;
		
		try {
			ZipFile zipFile = new ZipFile(archiveName);
			Enumeration<? extends ZipEntry> entries =  zipFile.entries();
			
			while (entries.hasMoreElements()) {
				
				ZipEntry zipEntry = entries.nextElement();
				InputStream input = zipFile.getInputStream(zipEntry);
				String fileName = zipEntry.getName();
				
				if (fileName.endsWith(".txt")) {
					BufferedReader br = new BufferedReader(new InputStreamReader(input));
					while((inputLine = br.readLine()) != null) {
						output += inputLine + "\n";
					}
					br.close();
				}
			}
			zipFile.close();
		} catch(IOException ex) {
			System.out.println("File not found!");
		}
		return output;
	}
}
