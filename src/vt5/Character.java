/**
 * Date: 26.9.2017
 * Character.java
 */
package vt5;

/**
 * @author Harri
 *
 */

class WeaponBehaviour {
	public void useWeapon() {
		System.out.println(this.getClass().getSimpleName().replaceAll("Behaviour", ""));
	}
}

class KnifeBehaviour extends WeaponBehaviour {
	
}

class SwordBehaviour extends WeaponBehaviour {
	
}

class ClubBehaviour extends WeaponBehaviour {
	
}

class AxeBehaviour extends WeaponBehaviour {
	
}

public class Character {
	public WeaponBehaviour weapon;
	
	
	public void fight() {
		System.out.print(this.getClass().getSimpleName() + " tappelee aseella ");
		weapon.useWeapon();	
	}
}

class Knight extends Character {
	
}

class King extends Character {
	
}

class Queen extends Character {
	
}

class Troll extends Character {
	
}

