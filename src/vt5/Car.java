/**
 * Date: 26.9.2017
 * Car.java
 */
package vt5;

/**
 * @author Harri
 *
 */
public class Car {
	
	private Body body;
	private Chassis chassis;
	private Engine engine;
	private int numOfWheels = 4;
	private Wheel[] wheels = new Wheel[numOfWheels];
	
	private class Body {
		private Body() {
			System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
		}
	}
	
	private class Chassis {
		private Chassis() {
			System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
		}
	}
	
	private class Engine {
		private Engine() {
			System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
		}
	}
	
	private class Wheel {
		private Wheel() {
			System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
		}
	}
	
	public Car() {
		body = new Body();
		chassis = new Chassis();
		engine = new Engine();
		for(int i = 0; i < wheels.length; i++) {
			wheels[i] = new Wheel();
		}
	}
	
	public void print() {
		System.out.println("Autoon kuuluu:");
		System.out.println("\t" + body.getClass().getSimpleName());
		System.out.println("\t" + chassis.getClass().getSimpleName());
		System.out.println("\t" + engine.getClass().getSimpleName());
		System.out.println("\t" + wheels.length + " " + wheels[0].getClass().getSimpleName());
	}
}

