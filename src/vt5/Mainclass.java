/**
 * Date: 26.9.2017
 * Mainclass.java
 */
package vt5;

import java.util.Scanner;

/**
 * @author Harri
 *
 */
public class Mainclass {

	public static void main(String[] args) {
		/*
		 Car myCar = new Car();
		 myCar.print();
		*/
		Scanner scan = new Scanner(System.in);
		String choice = "";
		Character hahmo = null;
		// Menu to run character selection
		do {
			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			
			choice = scan.nextLine();
			
			if (choice.equals("0")) {
				break;
			}else if (choice.equals("1")) {
				System.out.println("Valitse hahmosi: ");
				System.out.println("1) Kuningas");
				System.out.println("2) Ritari");
				System.out.println("3) Kuningatar");
				System.out.println("4) Peikko");
				System.out.print("Valintasi: ");
				
				String charChoice = scan.nextLine();
				
				if (charChoice.equals("1")) {
					hahmo = new King();
				}else if (charChoice.equals("2")) {
					hahmo = new Knight();
				}else if (charChoice.equals("3")) {
					hahmo = new Queen();
				}else if (charChoice.equals("4")) {
					hahmo = new Troll();
				}else {
					// TODO: Loop until get a character choice
					System.out.println("V��r� valinta!");
				}
				System.out.println("Valitse aseesi: ");
				System.out.println("1) Veitsi");
				System.out.println("2) Kirves");
				System.out.println("3) Miekka");
				System.out.println("4) Nuija");
				System.out.print("Valintasi: ");
				
				String wpnChoice = scan.nextLine();
				
				if (wpnChoice.equals("1")) {
					hahmo.weapon = new KnifeBehaviour();
				}else if (wpnChoice.equals("2")) {
					hahmo.weapon = new AxeBehaviour();
				}else if (wpnChoice.equals("3")) {
					hahmo.weapon = new SwordBehaviour();
				}else if (wpnChoice.equals("4")) {
					hahmo.weapon = new ClubBehaviour();
				}else {
					System.out.println("V��r� valinta!");
				}
			}else if (choice.equals("2")) {
				if(hahmo == null) {
					System.out.println("Luo ensin hahmo!");
				} else {
					hahmo.fight();
				}
				
			}else {
				System.out.println("V��r� valinta!");
			}
		}while (!choice.equals("0"));
		
		scan.close();
	}

}
