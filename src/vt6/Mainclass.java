/**
 * Mainclass.java
 * 27.9.2017
 */
package vt6;

import java.util.Scanner;

/**
 * @author Harri
 *
 */
public class Mainclass {

	public static void main(String[] args) {
		
		Bank bank = new Bank();
		Scanner scan = new Scanner(System.in);
		String choice = "";
		
		// Menu to run bank
		do {
			System.out.println("\n*** PANKKIJ�RJESTELM� ***");
			System.out.println("1) Lis�� tavallinen tili");
			System.out.println("2) Lis�� luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa");
			System.out.println("4) Nosta tililt�");
			System.out.println("5) Poista tili");
			System.out.println("6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			
			choice = scan.nextLine();
			
			if (choice.equals("0")) {
				break;
			}else if (choice.equals("1")) {
				System.out.print("Sy�t� tilinumero: ");
				String idString = scan.nextLine();
				System.out.print("Sy�t� raham��r�: ");
				int amount = Integer.parseInt(scan.nextLine());
				bank.addAccount(idString, amount);
				
				
			}else if (choice.equals("2")) {
				System.out.print("Sy�t� tilinumero: ");
				String idString = scan.nextLine();
				System.out.print("Sy�t� raham��r�: ");
				int amount = Integer.parseInt(scan.nextLine());
				System.out.print("Sy�t� luottoraja: ");
				int credit = Integer.parseInt(scan.nextLine());
				bank.addAccount(idString, amount, credit);
				
			}else if (choice.equals("3")) {
				System.out.print("Sy�t� tilinumero: ");
				String idString = scan.nextLine();
				System.out.print("Sy�t� raham��r�: ");
				int amount = Integer.parseInt(scan.nextLine());
				if(bank.searchAccount(idString) == null) {
					//System.out.println("Tili� ei l�ydy!");
					continue;
				}
				bank.searchAccount(idString).deposit(amount);
				
			}else if (choice.equals("4")) {
				System.out.print("Sy�t� tilinumero: ");
				String idString = scan.nextLine();
				System.out.print("Sy�t� raham��r�: ");
				int amount = Integer.parseInt(scan.nextLine());
				if(bank.searchAccount(idString) == null) {
					System.out.println("Tili� ei l�ydy!");
					continue;
				}
				bank.searchAccount(idString).withdraw(amount);
				
			}else if (choice.equals("5")) {
				System.out.print("Sy�t� poistettava tilinumero: ");
				String idString = scan.nextLine();
				if(bank.searchAccount(idString) == null) {
					//System.out.println("Tili� ei l�ydy!");
					continue;
				}
				bank.removeAccount(idString);
				
			}else if (choice.equals("6")) {
				System.out.print("Sy�t� tulostettava tilinumero: ");
				String idString = scan.nextLine();
				if(bank.searchAccount(idString) == null) {
					//System.out.println("Tili� ei l�ydy!");
					continue;
				}
				bank.searchAccount(idString).printInfo();
				
			}else if (choice.equals("7")) {
				System.out.println("Kaikki tilit:");
				bank.listAccounts();
				
			}else {
				System.out.println("Valinta ei kelpaa.");
			}
		}while (!choice.equals("0"));
		
		scan.close();
	}

}
