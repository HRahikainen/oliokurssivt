/**
 * Bank.java
 * 27.9.2017
 */
package vt6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Harri
 *
 */
public class Bank {
	private HashMap<String, Account> accountMap = new HashMap<String, Account>();
	// add different addAccounts for basic and credit
	public void addAccount(String id, int amount) {
		BasicAccount ba = new BasicAccount(id, amount);
		accountMap.put(id, ba);
		System.out.println("Tili luotu.");
	}
	
	public void addAccount(String id, int amount, int credit) {
		CreditAccount ca = new CreditAccount(id, amount, credit);
		accountMap.put(id, ca);
		System.out.println("Tili luotu.");
	}
	
	public void removeAccount(String id) {
		accountMap.remove(id);
		System.out.println("Tili poistettu.");
	}
	
	public Account searchAccount(String id) {
		//System.out.println("Etsit��n tili�: " + id);
		//return Account or null
		if(accountMap.get(id) != null) {
			return accountMap.get(id);
		}
		return null;
	}
	
	public void listAccounts() {
		//accountMap.forEach((k,v)->v.printInfo());
		/*for(Map.Entry<String, Account> e : accountMap.entrySet()) {
			e.getValue().printInfo();
		}*/
		Iterator it = accountMap.values().iterator();
		while(it.hasNext()){
			((Account)it.next()).printInfo();
		}
	}
}
