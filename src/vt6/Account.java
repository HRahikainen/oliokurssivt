/**
 * Account.java
 * 27.9.2017
 */
package vt6;


/**
 * @author Harri
 *
 */
public abstract class Account {
	protected String accountId;
	protected int balance;
	
	public Account(String id, int amount) {
		accountId = id;
		balance = amount;
	}
	
	
	public void deposit(int amount) {
		balance += amount;
	}
	
	public void withdraw(int amount) {
		if(balance < amount) {
			System.out.println("Tilill� ei tarpeeksi rahaa!");
			return;
		}
		balance -= amount;
	}
	public void printInfo() {
		System.out.println("Tilinumero: " + accountId + " Tilill� rahaa: " + balance);
	}
}

class BasicAccount extends Account {
	
	public BasicAccount(String id, int amount) {
		super(id, amount);
	}
}

class CreditAccount extends Account {
	private int creditLimit;
	
	public CreditAccount(String id, int amount, int credit) {
		super(id, amount);
		creditLimit = credit;
	}
	
	@Override
	public void withdraw(int amount) {
		if(creditLimit + balance >= amount) {
			balance -= amount;
		}else {
			System.out.println("Luottoraja ylitetty! Ei raheja!");
		}
	}
	@Override
	public void printInfo() {
		System.out.print("Tilinumero: " + accountId + " Tilill� rahaa: " + balance);
		System.out.println(" Luottoraja: " + creditLimit);
	}
}